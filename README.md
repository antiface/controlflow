ControlFlow
===========
News analysis dashboard reinvented.
The interface is wrong, the design is all wrong because the paradigm is wrong.
#Table of Contents
* [The Problem] (https://github.com/antiface/ControlFlow/tree/master/Problem-Statement)
* [The Control Center Paradigm] (https://github.com/antiface/ControlFlow/tree/master/The-Control-Center)
* [Enter The Screen] (https://github.com/antiface/ControlFlow/tree/master/Enter-The-Screen)
* [NewsBursts] (https://github.com/antiface/ControlFlow/tree/master/NewsBursts)
* [The Long Tail of Analysis] (https://github.com/antiface/ControlFlow/tree/master/Long-Tail-of-Analysis)
* [Top-of-Mind (ToM) Concept] (https://github.com/antiface/ControlFlow/tree/master/Top-of-Mind)

...work in progress, come back later...
